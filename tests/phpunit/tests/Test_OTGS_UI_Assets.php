<?php

/**
 * @author OnTheGo Systems
 */
class Test_OTGS_UI_Assets extends OTGS_TestCase {
	public function setUp() {
		parent::setUp();

		/** @var \WP_Filesystem_Base|\PHPUnit\Framework\MockObject\MockObject $wp_filesystem */
		global $wp_filesystem;

		$wp_filesystem = $this->getMockBuilder( '\WP_Filesystem_Base' )
							  ->disableOriginalConstructor()
							  ->setMethods( array( 'get_contents' ) )
							  ->getMock();

		$assets = file_get_contents( TESTS_DIR . '/resources/assets01.json' );

		$wp_filesystem->method( 'get_contents' )
					  ->willReturn( $assets );
	}

	public function tearDown() {
		unset( $GLOBALS['wp_filesystem'] );
		parent::tearDown();
	}

	/**
	 * @test
	 */
	public function it_registers_all_the_assets() {
		$assets_root_url = '/path/to/resources';

		$store = new OTGS_Assets_Store();
		$store->add_assets_location( TESTS_DIR . '/resources/assets01.json' );

		WP_Mock::userFunction( 'wp_register_script', array(
			'times' => 1,
			'args'  => array( 'handle-01', $assets_root_url . '/js/app.js?ver=123' ),
		) );
		WP_Mock::userFunction( 'wp_register_script', array(
			'times' => 1,
			'args'  => array( 'handle-02', $assets_root_url . '/js/app.js?ver=789' ),
		) );
		WP_Mock::userFunction( 'wp_register_script', array(
			'times' => 1,
			'args'  => array( 'handle-03-1', $assets_root_url . '/js/app1.js?ver=cde' ),
		) );
		WP_Mock::userFunction( 'wp_register_script', array(
			'times' => 1,
			'args'  => array( 'handle-03-2', $assets_root_url . '/js/app2.js?ver=fgh' ),
		) );

		WP_Mock::userFunction( 'wp_register_style', array(
			'times' => 1,
			'args'  => array( 'handle-01', $assets_root_url . '/css/styles.css?ver=456' ),
		) );
		WP_Mock::userFunction( 'wp_register_style', array(
			'times' => 1,
			'args'  => array( 'handle-02', $assets_root_url . '/css/styles.css?ver=0ab' ),
		) );

		$subject = new OTGS_UI_Assets( $assets_root_url, $store );
		$subject->register();
	}
}
