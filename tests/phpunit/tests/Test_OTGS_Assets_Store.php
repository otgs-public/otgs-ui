<?php

/**
 * @author OnTheGo Systems
 */
class Test_OTGS_Assets_Store extends OTGS_TestCase {

	/**
	 * @test
	 */
	public function it_does_NOT_attempt_to_parse_an_NO_existing_assets_file() {
		$subject = new OTGS_Assets_Store();
		$subject->add_assets_location( TESTS_DIR . '/resources/non-existing-assets.json');

		$this->assertCount( 0, $subject->get( 'js' ) );
		$this->assertCount( 0, $subject->get( 'css' ) );

		$this->assertCount( 0, $subject->get( 'js', 'handle-01' ) );
		$this->assertCount( 0, $subject->get( 'css', 'handle-01' ) );
	}

	/**
	 * @test
	 */
	public function it_parses_an_assets_file() {
		$subject = new OTGS_Assets_Store();
		$subject->add_assets_location( TESTS_DIR . '/resources/assets01.json');

		$this->assertCount( 3, $subject->get( 'js' ) );
		$this->assertCount( 2, $subject->get( 'css' ) );

		$this->assertCount( 1, $subject->get( 'js', 'handle-01' ) );
		$this->assertCount( 1, $subject->get( 'css', 'handle-01' ) );

		$this->assertCount( 1, $subject->get( 'js', 'handle-02' ) );
		$this->assertCount( 1, $subject->get( 'css', 'handle-02' ) );

		$this->assertCount( 2, $subject->get( 'js', 'handle-03' ) );
		$this->assertCount( 0, $subject->get( 'css', 'handle-03' ) );
	}
}
