<?php

/**
 * @author OnTheGo Systems
 */
class Test_OTGS_UI_Loader extends OTGS_TestCase {
	/**
	 * @test
	 * @expectedException \InvalidArgumentException
	 * @expectedExceptionMessage Missing assets and assets store
	 */
	public function it_throws_an_exception_if_missing_dependencies() {
		new OTGS_UI_Loader();
	}

	private function get_subject() {
		/** @var \OTGS_Assets_Store|\PHPUnit_Framework_MockObject_MockObject $store */
		$store = \Mockery::mock( 'OTGS_Assets_Store' );
		/** @var \OTGS_UI_Assets|\PHPUnit_Framework_MockObject_MockObject $assets */
		$assets = \Mockery::mock( 'OTGS_UI_Assets' );
		$subject = new OTGS_UI_Loader( $store, $assets );

		return $subject;
	}

	/**
	 * @test
	 */
	public function it_does_NOT_hooks_to_init_action() {
		$subject = $this->get_subject();

		WP_Mock::userFunction( 'has_action', array( 'return' => true ) );
		WP_Mock::expectActionNotAdded( 'init', array( $subject, 'register' ), 1 );

		$subject->load();
	}

	/**
	 * @test
	 */
	public function it_hooks_to_init_action() {
		$subject = $this->get_subject();

		WP_Mock::userFunction( 'has_action', array( 'return' => false ) );
		WP_Mock::expectActionAdded( 'init', array( $subject, 'register' ), 1 );

		$subject->load();
	}
}
